# LibreCademy - API-REST

LibreCademy It's an open source initiative to share knowledge for free for all, share video tutorials, whole courses, posts and more.

This repository is dedicated to backend

## Getting started

- Clone this repository
- Setup the project

```bash
rails db:create
rails db:migrate
rails db:seed
```

- Tests

```bash
bundle exec rspec
```

## API Documentation

[Online Documentation](https://documenter.getpostman.com/view/4266949/RWTmwKP4)

## Licence

[MIT](https://gitlab.com/luishck/librecademy/blob/master/LICENSE)

Luis J. Centeno - 2018

Made with ❤️