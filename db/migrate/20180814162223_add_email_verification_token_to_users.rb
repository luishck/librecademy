class AddEmailVerificationTokenToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :email_verification_token, :string
  end
end
