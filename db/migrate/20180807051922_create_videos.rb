class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string :title
      t.text :description
      t.integer :list_index
      t.string :url
      t.string :duration
      t.string :state
      t.string :author
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
