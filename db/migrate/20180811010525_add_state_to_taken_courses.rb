class AddStateToTakenCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :taken_courses, :state, :string
  end
end
