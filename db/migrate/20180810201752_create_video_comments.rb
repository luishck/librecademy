class CreateVideoComments < ActiveRecord::Migration[5.2]
  def change
    create_table :video_comments do |t|
      t.references :user, foreign_key: true
      t.references :video, foreign_key: true
      t.text :content
      t.string :state
      t.boolean :top, default: false

      t.timestamps
    end
  end
end
