class AddFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :email_state, :string, default: "pending"
    add_column :users, :github, :string
  end
end
