class CreateStudyMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :study_materials do |t|
      t.string :title
      t.string :description
      t.string :url
      t.string :git
      t.string :files
      t.references :user, foreign_key: true
      t.references :video, foreign_key: true

      t.timestamps
    end
  end
end
