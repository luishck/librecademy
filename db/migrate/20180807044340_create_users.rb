class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.string :username
      t.string :email
      t.string :phone
      t.string :facebook_id
      t.string :google_id
      t.string :cable_token
      t.string :uid
      t.string :state
      t.string :password_digest

      t.timestamps
    end
  end
end
