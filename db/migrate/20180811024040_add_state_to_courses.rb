class AddStateToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :state, :string, default: "draft"
  end
end
