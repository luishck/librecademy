class AddMoreFieldsToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :creator, :string
    add_column :courses, :verified, :boolean
    add_column :courses, :top, :boolean
  end
end
