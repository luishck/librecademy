# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users = [
  {
    name: "User",
    lastname: "Test",
    email: "user@libreacademy.com",
    password: "libre_user_1",
    username: "test_user_1",
  },
]

if User.create(users)
  puts("Created users")
end

courses = [
  {
    title: "Learning Javascript from Scratch",
    description: "We will learning the basis, then we create a gigant app using jquery",
    creator: "platzi",
    user: User.last,
  },
  {
    title: "Creating shit with php",
    description: "Please don't do anything with php, just uninstall it",
    creator: "codigofacilito",
    user: User.last,
  },
]

Course.create(courses)

videos = [
  {
    title: "Jquery is better than React",
    description: "Jquery is the worlds leading techonology",
    course: Course.first,
  },
  {
    title: "You don't need Vuejs",
    description: " VueJS is bad because is created by a chinese guy (or japanese?) and he think is You",
    course: Course.first,
  },
  {
    title: "Install php 3 in windows xp",
    description: "PHP 7 is shit, we need something a bit retro, also windows 10 is written with python",
    course: Course.second,
  },
  {
    title: "How to deal with php sql injection",
    description: "SQL injection is normal, all pages are sql injected, nothing to worry about",
    course: Course.second,
  },
]

Video.create(videos)
