FactoryBot.define do
  factory :user do
    name { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    email { Faker::Internet.email }
    uid { SecureRandom.hex(11) }
    username { Faker::Internet.username }
    password "fake_password-xd"
  end

  factory :invalid_user, class: User do
    name nil
    lastname nil
    email nil
    uid nil
    username nil
    password nil
  end
end
