FactoryBot.define do
  factory :valid_study_material, class: StudyMaterial do
    title { Faker::Lorem.sentence}
    description { Faker::Lorem.sentence}
    url { Faker::Internet.url("somesiteoninternet.com") }
    git { Faker::Internet.url("github.com") }
  end

  factory :invalid_study_material, class: StudyMaterial do
    title nil
    description nil
    url nil
    git nil
  end
end
