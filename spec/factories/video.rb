FactoryBot.define do
  factory :valid_video, class: Video do
    title { Faker::Lorem.sentence}
    description { Faker::Lorem.sentence}
    list_index 1
    url { Faker::Internet.url("youtube.com") }
    duration "13:30"
    author { Faker::Lorem.sentence}
    course_id 1
  end

  factory :video, class: Video do
    title { Faker::Lorem.sentence}
    description { Faker::Lorem.sentence}
    list_index 1
    url { Faker::Internet.url("youtube.com") }
    duration "13:30"
    author { Faker::Lorem.sentence}
    course_id 1
  end
end
