FactoryBot.define do
  factory :valid_taken_course, class: TakenCourse do
    course_id { }
    user_id { }
  end
  factory :taken_course, class: TakenCourse do
    course_id { }
    user_id { }
  end
end
