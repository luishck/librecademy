FactoryBot.define do
  factory :valid_course, class: Course do
    title { Faker::Lorem.sentence}
    description { Faker::Lorem.sentence}
    creator { Faker::Lorem.characters(10) }
  end

  factory :course, class: Course do
    title { Faker::Lorem.sentence}
    description { Faker::Lorem.sentence}
    creator { Faker::Lorem.characters(10) }
  end

  factory :invalid_course, class: Course do
    title nil
    description nil
    creator nil
  end
end
