FactoryBot.define do
  factory :video_comment do
    user nil
    video nil
    content "MyText"
    state "published"
  end
end
