FactoryBot.define do
  factory :annotation, class: Annotation do
    comment { Faker::Lorem.sentence }
    video_time "1:30"
    user nil
    video nil
  end
end
