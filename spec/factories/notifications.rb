FactoryBot.define do
  factory :notification do
    header { Faker::Lorem.sentence}
    content { Faker::Lorem.sentence}
    kind { Faker::Lorem.sentence}
    read false
    user nil
  end
end
