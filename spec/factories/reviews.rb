FactoryBot.define do
  factory :review do
    rate { Faker::Number.between(1, 5) }
    comment { Faker::Lorem.sentence}
    state "published"
    user nil
    course nil
  end
end
