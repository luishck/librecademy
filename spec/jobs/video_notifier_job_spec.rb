require "rails_helper"

RSpec.describe VideoNotifierJob, type: :job do
  describe "#perform_later" do
    it "Notify all course students for new video" do
      ActiveJob::Base.queue_adapter = :test
      expect {
        VideoNotifierJob.perform_later(create(:course, user: create(:user)))
      }.to have_enqueued_job
    end
  end
end
