require "rails_helper"

RSpec.describe ValidationMailer, type: :mailer do
  describe "Verify" do
    let(:user) { create(:user) }
    let(:mail) { ValidationMailer.account_verification(user) }

    it "renders the headers" do
      expect(mail.subject).to eq(I18n.t("mailer.account_verification.subject"))
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq(["librecademy@gmail.com"])
    end
  end
end
