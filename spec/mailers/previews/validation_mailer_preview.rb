# Preview all emails at http://localhost:3000/rails/mailers/validation_mailer
class ValidationMailerPreview < ActionMailer::Preview
  def method_name
    ValidationMailer.account_verification(User.first)
  end
end
