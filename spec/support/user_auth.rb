def user_auth(user)
  token = Knock::AuthToken.new(payload: {sub: user.id}).token
  return {'Authorization': "#{token}"}
end

def set_auth(req, user)
  req.headers.merge!(user_auth(user))
end
