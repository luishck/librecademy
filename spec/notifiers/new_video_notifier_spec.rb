require "rails_helper"

RSpec.describe NewVideoNotifier do
  describe "#New video addedd" do
    let(:student_1) { create :user }
    let(:student_2) { create :user }
    let(:course) { create(:course, user: create(:user)) }

    it "generate a notification for each student" do
      student_1.taken_courses.create(course: course)
      student_2.taken_courses.create(course: course)
      create(:video, course_id: course.id)

      expect(NewVideoNotifier.notify_students(course)).to eq true
    end
  end
end
