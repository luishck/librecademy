require "rails_helper"

RSpec.describe "Courses", type: :request do
  let (:valid_user_attributes) { FactoryBot.attributes_for(:user) }

  describe "GET /courses" do
    it "show all courses without filter" do
      user = User.create! valid_user_attributes
      headers = user_auth(user)

      get courses_path, headers: headers
      expect(response).to have_http_status(200)
    end
  end
end
