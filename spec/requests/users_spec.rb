require "rails_helper"

RSpec.describe "Users", type: :request do
  let(:valid_attributes) { FactoryBot.attributes_for(:user) }

  describe "GET /users" do
    context "Request as student" do
      it "allowed to see profile" do
        user = User.create! valid_attributes
        headers = user_auth(user)

        get "/users", headers: headers
        expect(response.content_type).to eq("application/json")
        expect(response).to have_http_status(200)
      end

      it "a student can see other student profile" do
        student_1 = User.create! FactoryBot.attributes_for(:user)
        student_2 = User.create! FactoryBot.attributes_for(:user)

        headers = user_auth(student_1)

        get "/users/#{student_2.id}", headers: headers
        expect(response.content_type).to eq("application/json")
        expect(response).to have_http_status(200)
      end
    end

    context "Request some user as admin" do
      it "return a 200 status" do
        admin = User.create! FactoryBot.attributes_for(:user)
        admin.add_role(:admin)

        student_1 = User.create! FactoryBot.attributes_for(:user)

        headers = user_auth(admin)

        get "/users/#{student_1.id}", headers: headers
        expect(response.content_type).to eq("application/json")
        expect(response).to have_http_status(200)
      end
    end
  end
end
