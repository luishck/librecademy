require "rails_helper"

RSpec.describe "StudyMaterials", type: :request do
  let(:valid_user) { create(:user) }
  let(:valid_course) { create(:valid_course, user_id: valid_user.id) }
  let(:valid_video) { create(:valid_video, course_id: valid_course.id) }

  describe "GET /study_materials" do
    it "works! (now write some real specs)" do
      get "/courses/#{valid_course.id}/videos/#{valid_video.id}/study_materials", headers: user_auth(valid_user)
      expect(response).to have_http_status(200)
    end
  end
end
