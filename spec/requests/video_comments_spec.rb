require "rails_helper"

RSpec.describe "VideoComments", type: :request do
  let(:user) { create(:user) }
  let(:course) { create(:course, user_id: user.id) }
  let(:video) { create(:video, course_id: course.id) }
  let(:video_comment) {
    create(
      :video_comment,
      video_id: video.id,
      user_id: user.id,
    )
  }

  describe "GET /video_comments" do
    it "works! (now write some real specs)" do
      get "/courses/#{course.id}/videos/#{video.id}/", headers: user_auth(user)
      expect(response).to have_http_status(200)
    end
  end
end
