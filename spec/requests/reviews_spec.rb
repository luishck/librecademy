require "rails_helper"

RSpec.describe "Reviews", type: :request do
  let(:user) { create(:user) }
  let(:course) { create(:course, user: user) }

  describe "GET /reviews" do
    it "works! (now write some real specs)" do
      get "/courses/#{course.id}/reviews", headers: user_auth(user)
      expect(response).to have_http_status(200)
    end
  end
end
