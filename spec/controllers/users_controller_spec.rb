require "rails_helper"

RSpec.describe UsersController, type: :controller do
  let(:valid_attributes) { FactoryBot.attributes_for(:user) }
  let(:new_attributes) { FactoryBot.attributes_for(:user) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:invalid_user) }

  describe "GET #index" do
    it "returns a success response" do
      user = User.create! valid_attributes
      set_auth(request, user)
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      user = User.create! valid_attributes
      set_auth(request, user)
      get :show, params: {id: user.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new User" do
        expect {
          post :create, params: {user: valid_attributes}
        }.to change(User, :count).by(1)
      end

      it "renders a JSON response with the new user" do
        post :create, params: {user: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
        expect(response.location).to eq(user_url(User.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new user" do
        post :create, params: {user: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      it "updates the requested user" do
        user = User.create! valid_attributes
        set_auth(request, user)
        put :update, params: {id: user.to_param, user: new_attributes}
        user.reload
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the user" do
        user = User.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: user.to_param, user: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the user" do
        user = User.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: user.to_param, user: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "verify user email" do
      it "successful verification" do
        user = User.create! valid_attributes
        token = user.email_verification_token

        set_auth(request, user)
        put :update, params: {
                               id: user.to_param,
                               req_action: "verify_email",
                               email_verification_token: token,
                               user: {id: user.id},
                             }
        user.reload
        expect(user.email_state).to eq "verified"
        expect(response).to have_http_status(:ok)
      end

      it "fail verification" do
        user = User.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                               id: user.to_param,
                               req_action: "verify_email",
                               email_verification_token: "invalid_token",
                               user: {id: user.id},
                             }
        user.reload
        expect(user.email_state).to eq "pending"
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "creator role request" do
      it "successful request when user is verified" do
        user = create(:user)
        user.verify_email!

        set_auth(request, user)
        put :update, params: {
                       id: user.to_param,
                       req_action: "request_creator",
                       user: {id: user.id},
                     }
        user.reload

        expect(user.has_role?(:creator)).to eq true
        expect(response).to have_http_status(:ok)
      end

      it "fail request when user is unverified" do
        user = create(:user)

        set_auth(request, user)
        put :update, params: {
                       id: user.to_param,
                       req_action: "request_creator",
                       user: {id: user.id},
                     }
        user.reload

        expect(user.has_role?(:creator)).to eq false
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested user" do
      user = User.create! valid_attributes

      set_auth(request, user)
      expect {
        delete :destroy, params: {id: user.to_param}
      }.to change(User, :count).by(-1)
    end
  end
end
