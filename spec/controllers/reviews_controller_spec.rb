require "rails_helper"

RSpec.describe ReviewsController, type: :controller do
  # Initialize user and course
  let(:user) { create(:user) }
  let(:course) { create(:course, user: user) }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:review, user: user, course: course)
  }
  let(:invalid_attributes) {
    FactoryBot.attributes_for(:review, user: user, course: course, comment: nil, rate: -1)
  }

  describe "GET #index" do
    it "returns a success response" do
      set_auth(request, user)
      get :index, params: {course_id: course.id}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      review = Review.create! valid_attributes

      set_auth(request, user)
      get :show, params: {course_id: course.id, id: review.id}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Review" do
        set_auth(request, user)
        expect {
          post :create, params: {course_id: course.id, review: valid_attributes}
        }.to change(Review, :count).by(1)
      end

      it "renders a JSON response with the new review" do
        set_auth(request, user)
        post :create, params: {course_id: course.id,
                               review: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new review" do
        set_auth(request, user)
        post :create, params: {course_id: course.id,
                               review: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {comment: "updated_comment", rate: 5}
      }

      it "updates the requested review" do
        review = Review.create! valid_attributes

        set_auth(request, user)
        put :update, params: {course_id: course.id,
                              id: review.to_param, review: new_attributes}
        review.reload
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the review" do
        review = Review.create! valid_attributes

        set_auth(request, user)
        put :update, params: {course_id: course.id,
                              id: review.to_param, review: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the review" do
        review = Review.create! valid_attributes

        set_auth(request, user)
        put :update, params: {course_id: course.id,
                              id: review.to_param, review: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "a private review" do
      let(:new_attributes) { {comment: "new comment", rate: 1} }
      let(:private_review) { create(:review, user: create(:user), course: course) }
      it "it fails if it is not the owner" do
        set_auth(request, user)
        put :update, params: {
                       course_id: course.id,
                       id: private_review.to_param,
                       review: new_attributes,
                     }
        expect(response).to have_http_status(:forbidden)
      end

      it "it works if it is Admin or Moderator" do
        # Initialize super admin
        admin = create(:user)
        admin.add_role(:admin)
        admin.add_role(:moderator)

        set_auth(request, admin)
        put :update, params: {
                       course_id: course.id,
                       id: private_review.to_param,
                       review: new_attributes,
                     }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested review" do
      review = Review.create! valid_attributes
      set_auth(request, user)
      expect {
        delete :destroy, params: {course_id: course.id,
                                  id: review.to_param}
      }.to change(Review, :count).by(-1)
    end

    it "destroys the requested review as Admin" do
      # Initialize super admin
      admin = create(:user)
      admin.add_role(:admin)
      admin.add_role(:moderator)

      review = Review.create! valid_attributes

      set_auth(request, admin)
      expect {
        delete :destroy, params: {course_id: course.id,
                                  id: review.to_param}
      }.to change(Review, :count).by(-1)
    end

    it "can't destroy another's review" do
      review = Review.create! valid_attributes

      set_auth(request, create(:user))
      expect {
        delete :destroy, params: {course_id: course.id,
                                  id: review.to_param}
      }.to change(Review, :count).by(0)
    end
  end
end
