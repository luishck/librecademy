require "rails_helper"

RSpec.describe AnnotationsController, type: :controller do
  let(:user) { create(:user) }

  # Build video
  let(:course) { create(:course, user: create(:user)) }
  let(:video) { create(:video, course: course) }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:annotation, user: user, video_id: video.id)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:annotation, comment: nil, video_time: nil, user: user, video: video)
  }

  describe "GET #index" do
    it "returns a success response" do
      set_auth(request, user)
      annotation = Annotation.create! valid_attributes
      get :index, params: {
                            user_id: user.id,
                          }
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      annotation = Annotation.create! valid_attributes
      get :show, params: {
                   user_id: user.to_param,
                   id: annotation.to_param,
                 }
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Annotation" do
        set_auth(request, user)
        expect {
          post :create, params: {
                          user_id: user.id,
                          annotation: valid_attributes,
                        }
        }.to change(Annotation, :count).by(1)
      end

      it "renders a JSON response with the new annotation" do
        set_auth(request, user)
        post :create, params: {
                        user_id: user.id,
                        annotation: valid_attributes,
                      }
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new annotation" do
        set_auth(request, user)
        post :create, params: {
                        user_id: user.id,
                        annotation: invalid_attributes,
                      }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryBot.attributes_for(:annotation, comment: "updated comment")
      }

      it "updates the requested annotation" do
        annotation = Annotation.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       user_id: user.id,
                       id: annotation.to_param,
                       annotation: new_attributes,
                     }
        annotation.reload
        expect(annotation.comment).to eq("updated comment")
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the annotation" do
        annotation = Annotation.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       user_id: user.id,
                       id: annotation.to_param, annotation: valid_attributes,
                     }
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the annotation" do
        annotation = Annotation.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       user_id: user.id,
                       id: annotation.to_param, annotation: invalid_attributes,
                     }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested annotation" do
      annotation = Annotation.create! valid_attributes
      set_auth(request, user)
      expect {
        delete :destroy, params: {
                                   user_id: user.id,
                                   id: annotation.to_param,
                                 }
      }.to change(Annotation, :count).by(-1)
    end
  end
end
