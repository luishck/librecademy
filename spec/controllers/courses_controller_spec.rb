require "rails_helper"

RSpec.describe CoursesController, type: :controller do
  let(:valid_user_attributes) { FactoryBot.attributes_for(:user) }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:valid_course)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:invalid_course)
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CoursesController. Be sure to keep this updated too.

  describe "GET #index" do
    it "returns a success response" do
      user = User.create! valid_user_attributes
      course = user.courses.create! valid_attributes

      set_auth(request, user)
      get :index, params: {}
      expect(response).to be_successful
    end

    # Tests for filtering system
    #
    it "filter by creator" do
      user = User.create! valid_user_attributes
      course = create(:valid_course, creator: "platzi", user_id: user.id)
      course_2 = create(:valid_course, creator: "codigofacilito", user_id: user.id)

      set_auth(request, user)
      get :index, params: {creator: "platzi"}
      expect(assigns[:courses].size).to eq 1
      expect(response).to be_successful
    end

    it "filter by valid an array of valid IDs" do
      user = User.create! valid_user_attributes
      course_1 = create(:valid_course, creator: "platzi", user_id: user.id)
      course_2 = create(:valid_course, creator: "platzi", user_id: user.id)

      set_auth(request, user)
      get :index, params: {course_ids: [course_1.id, course_2.id]}

      expect(assigns[:courses].size).to eq 2
      expect(response).to be_successful
    end

    it "filter by invalid array of IDs" do
      user = User.create! valid_user_attributes
      course_1 = create(:valid_course, creator: "platzi", user_id: user.id)
      course_2 = create(:valid_course, creator: "platzi", user_id: user.id)

      set_auth(request, user)
      get :index, params: {course_ids: [99, 5]}

      expect(assigns[:courses].size).to eq 0
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      user = User.create! valid_user_attributes
      course = user.courses.create! valid_attributes

      set_auth(request, user)
      get :show, params: {id: course.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        set_auth(request, user)

        expect {
          post :create, params: {course: valid_attributes}
        }.to change(Course, :count).by(1)
        expect(response).to have_http_status(:created)
      end

      it "renders a JSON response with the new course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        set_auth(request, user)

        post :create, params: {course: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
        expect(response.location).to eq(course_url(Course.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        set_auth(request, user)

        post :create, params: {course: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryBot.attributes_for(:valid_course, title: "new title", description: "new description")
      }

      it "updates the requested course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        course = user.courses.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: course.to_param, course: new_attributes}
        course.reload
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        set_auth(request, user)

        course = user.courses.create! valid_attributes

        put :update, params: {id: course.to_param, course: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      # AASM Validations
      it "changes state from draft to published by creator" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        course = user.courses.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: course.id, state_action: "to_published!", course: {id: course.id}}
        expect(response).to have_http_status(:ok)
      end

      it "Creator can't publish another's course" do
        user = create(:user)
        user.add_role(:creator)

        # Private course
        other = create(:user)
        course = other.courses.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: course.id, state_action: "to_published!", course: {id: course.id}}
        expect(response).to have_http_status(:forbidden)
      end

      it "Admin can set state banned for any course" do
        admin = create(:user, name: "Administrador")
        admin.add_role(:admin)

        course = create(:course, user: create(:user))

        set_auth(request, admin)
        put :update, params: {id: course.id, state_action: "to_banned!", course: {id: course.id}}
        expect(response).to have_http_status(:ok)
        expect(assigns[:course].state).to eq "banned"
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the course" do
        user = User.create! valid_user_attributes
        user.add_role(:creator)
        set_auth(request, user)

        course = user.courses.create! valid_attributes

        put :update, params: {id: course.to_param, course: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested course" do
      user = User.create! valid_user_attributes
      user.add_role(:creator)
      set_auth(request, user)

      course = user.courses.create! valid_attributes
      expect {
        delete :destroy, params: {id: course.to_param}
      }.to change(Course, :count).by(-1)
    end
  end
end
