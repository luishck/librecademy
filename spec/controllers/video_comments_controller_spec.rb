require "rails_helper"

def set_auth(request, user)
  request.headers.merge!(user_auth(user))
end

RSpec.describe VideoCommentsController, type: :controller do
  # Static stuff
  let(:user) { create(:user) }
  let(:course) { create(:valid_course, user_id: user.id) }
  let(:video) { create(:valid_video, course_id: course.id) }

  # Variable stuff for testing
  let(:valid_attributes) {
    FactoryBot.attributes_for(:video_comment, user: user)
  }
  let(:invalid_attributes) {
    FactoryBot.attributes_for(:video_comment, content: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      video_comment = video.video_comments.create! valid_attributes

      set_auth(request, user)
      get :index, params: {
                    course_id: course.id,
                    id: video.id,
                  }
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      video_comment = video.video_comments.create! valid_attributes

      set_auth(request, user)
      get :show, params: {
                   course_id: course.id,
                   video_id: video.id,
                   id: video_comment.id,
                 }
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new VideoComment" do
        set_auth(request, user)
        expect {
          post :create, params: {
                          course_id: course.id,
                          id: video.id,
                          video_comment: valid_attributes,
                        }
        }.to change(VideoComment, :count).by(1)
      end

      it "renders a JSON response with the new video_comment" do
        set_auth(request, user)

        post :create, params: {
                        course_id: course.id,
                        id: video.id,
                        video_comment: valid_attributes,
                      }
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new video_comment" do
        set_auth(request, user)
        post :create, params: {
                        course_id: course.id,
                        id: video.id,
                        video_comment: invalid_attributes,
                      }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {content: "new content update"}
      }

      it "updates the requested video_comment" do
        set_auth(request, user)
        video_comment = video.video_comments.create! valid_attributes

        put :update, params: {
                       course_id: course.id,
                       video_id: video.id,
                       id: video_comment.id,
                       video_comment: new_attributes,
                     }
        video_comment.reload
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the video_comment" do
        video_comment = video.video_comments.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       course_id: course.id,
                       video_id: video.id,
                       id: video_comment.id,
                       video_comment: valid_attributes,
                     }
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the video_comment" do
        video_comment = video.video_comments.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       course_id: course.id,
                       video_id: video.id,
                       id: video_comment.id,
                       video_comment: invalid_attributes,
                     }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested video_comment" do
      video_comment = video.video_comments.create! valid_attributes

      set_auth(request, user)
      expect {
        delete :destroy, params: {
                           course_id: course.id,
                           video_id: video.id,
                           id: video_comment.id,
                         }
      }.to change(VideoComment, :count).by(-1)
    end
  end
end
