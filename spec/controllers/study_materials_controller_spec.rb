require "rails_helper"

RSpec.describe StudyMaterialsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # StudyMaterial. As you add validations to StudyMaterial, be sure to
  # adjust the attributes here as well.

  let(:invalid_study_material) { FactoryBot.attributes_for(:invalid_study_material) }

  let(:valid_user) { create(:user) }

  let(:valid_course) { create(:valid_course, user_id: valid_user.id) }

  let(:valid_video) { create(:valid_video, course_id: valid_course.id) }

  let(:valid_study_material) {
    FactoryBot.attributes_for(:valid_study_material, user_id: valid_user.id, video_id: valid_video.id)
  }

  let(:invalid_study_material) {
    FactoryBot.attributes_for(:invalid_study_material)
  }

  # describe "GET #index" do
  #   it "returns a success response" do
  #     study_material = valid_video.study_materials.create! valid_study_material
  #     get :index, params: {}
  # set_auth(request,valid_user)
  #     expect(response).to be_successful
  #   end
  # end

  describe "GET #show" do
    it "returns a success response" do
      study_material = valid_video.study_materials.create! valid_study_material

      set_auth(request, valid_user)
      get :show, params: {
                   course_id: valid_course.id,
                   video_id: valid_video.id,
                   id: study_material.id,
                 }

      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new StudyMaterial" do
        valid_user.add_role(:creator)

        set_auth(request, valid_user)
        expect {
          post :create, params: {course_id: valid_course.id, id: valid_video.id, study_material: valid_study_material}
        }.to change(StudyMaterial, :count).by(1)
      end

      it "renders a JSON response with the new study_material" do
        set_auth(request, valid_user)

        post :create, params: {course_id: valid_course.id, id: valid_video.id, study_material: valid_study_material}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
      end

      it "creates a new StudyMaterial with multiple files" do
        user = create(:user)
        user.add_role :creator
        files =
          fixture_file_upload(Rails.root.join(
            "public/test_files",
            "images.jpeg"
          ), "image/jpeg")

        set_auth(request, user)
        post :create, params: {
                        course_id: valid_course.id,
                        id: valid_video.id,
                        study_material: FactoryBot.attributes_for(
                          :valid_study_material,
                          user_id: user.id,
                          video_id: valid_video.id,
                          files: files,
                        ),
                      }
        expect(response).to have_http_status(:created)
        expect(StudyMaterial.last.files.attached?).to eq true
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new study_material" do
        set_auth(request, valid_user)

        post :create, params: {
                        course_id: valid_course.id,
                        id: valid_video.id,
                        study_material: invalid_study_material,
                      }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          study_material: {
            title: "new title",
            description: "new description",
          },
        }
      }

      it "updates the requested study_material" do
        study_material = valid_video.study_materials.create! valid_study_material

        valid_user.add_role(:creator)

        set_auth(request, valid_user)
        put :update, params: {
                       course_id: valid_course.id,
                       video_id: valid_video.id,
                       id: study_material.id,
                       study_material: new_attributes,
                     }

        study_material.reload
        expect(response).to have_http_status(:ok)
      end

      it "renders a JSON response with the study_material" do
        study_material = valid_video.study_materials.create! valid_study_material

        set_auth(request, valid_user)
        put :update, params: {
                       course_id: valid_course.id,
                       video_id: valid_video.id,
                       id: study_material.id,
                       study_material: valid_study_material,
                     }

        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the study_material" do
        study_material = valid_video.study_materials.create! valid_study_material

        set_auth(request, valid_user)
        put :update, params: {
                       course_id: valid_course.id,
                       video_id: valid_video.id,
                       id: study_material.id,
                       study_material: invalid_study_material,
                     }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested study_material" do
      study_material = valid_video.study_materials.create! valid_study_material
      expect {
        set_auth(request, valid_user)

        delete :destroy, params: {
                           course_id: valid_course.id,
                           video_id: valid_video.id,
                           id: study_material.id,
                         }
      }.to change(StudyMaterial, :count).by(-1)
    end
  end
end
