require "rails_helper"

RSpec.describe TakenCoursesController, type: :controller do
  let(:valid_user) { create(:user) }
  let(:valid_course) { create(:valid_course, user_id: valid_user.id) }

  let(:valid_attributes) {
    FactoryBot.attributes_for(
      :valid_taken_course,
      course_id: valid_course.id,
      user_id: valid_user.id,
    )
  }

  let(:invalid_attributes) { FactoryBot.attributes_for(:valid_taken_course, course_id: 12) }

  describe "GET #index" do
    it "returns a success response" do
      taken_course = TakenCourse.create! valid_attributes

      set_auth(request, valid_user)
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      taken_course = TakenCourse.create! valid_attributes

      set_auth(request, valid_user)
      get :show, params: {
                   id: taken_course.id,
                 }
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new TakenCourse" do
        set_auth(request, valid_user)
        expect {
          post :create, params: {
                          taken_course: valid_attributes,
                        }
        }.to change(TakenCourse, :count).by(1)
      end

      it "try to duplicate a TakenCourse" do
        taken_course = TakenCourse.create! valid_attributes

        set_auth(request, valid_user)
        post :create, params: {
                        taken_course: valid_attributes,
                      }
        expect(response).to have_http_status(:ok)
      end

      it "renders a JSON response with the new taken_course" do
        set_auth(request, valid_user)
        post :create, params: {
                        taken_course: valid_attributes,
                      }

        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new taken_course" do
        set_auth(request, valid_user)
        post :create, params: {
                        taken_course: invalid_attributes,
                      }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {state: "finished"}
      }

      it "updates the requested taken_course" do
        taken_course = TakenCourse.create! valid_attributes

        set_auth(request, valid_user)
        put :update, params: {
                       id: taken_course.id,
                       taken_course: new_attributes,
                     }
        taken_course.reload

        expect(taken_course.state).to eq("finished")
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the taken_course" do
        taken_course = TakenCourse.create! valid_attributes

        set_auth(request, valid_user)
        put :update, params: {
                       id: taken_course.to_param,
                       taken_course: valid_attributes,
                     }

        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the taken_course" do
        taken_course = TakenCourse.create! valid_attributes

        set_auth(request, valid_user)
        put :update, params: {
                       id: taken_course.to_param,
                       taken_course: invalid_attributes,
                     }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested taken_course" do
      taken_course = TakenCourse.create! valid_attributes

      set_auth(request, valid_user)
      expect {
        delete :destroy, params: {id: taken_course.to_param}
      }.to change(TakenCourse, :count).by(-1)
    end
  end
end
