require "rails_helper"

RSpec.describe NotificationsController, type: :controller do
  let(:user) { create :user }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:notification, user_id: user.id)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(
      :notification,
      header: nil,
      content: nil,
      read: "false",
      user_id: user.id,
    )
  }

  describe "GET #index" do
    it "returns a success response" do
      notification = user.notifications.create! valid_attributes
      set_auth(request, user)
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      notification = user.notifications.create! valid_attributes

      set_auth(request, user)
      get :show, params: {id: notification.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "normal user can't create notifications" do
        set_auth(request, user)

        post :create, params: {notification: valid_attributes}
        expect(response).to have_http_status(:forbidden)
      end

      it "admin user can create notifications" do
        user.add_role(:admin)
        set_auth(request, user)

        post :create, params: {notification: valid_attributes}
        expect(response).to have_http_status(:created)
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new notification" do
        user.add_role(:admin)
        set_auth(request, user)
        post :create, params: {notification: invalid_attributes}

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          header: "new header",
          content: "new content",
        }
      }

      it "admin updates the requested notification" do
        notification = user.notifications.create! valid_attributes

        user.add_role(:admin)
        set_auth(request, user)
        put :update, params: {
                       id: notification.to_param,
                       notification: new_attributes,
                     }
        notification.reload

        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "renders a JSON response with the notification for admins" do
        notification = user.notifications.create! valid_attributes
        user.add_role(:admin)
        set_auth(request, user)
        put :update, params: {
                       id: notification.to_param,
                       notification: valid_attributes,
                     }
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end

      it "normal users can't update others notifications" do
        notification = user.notifications.create! valid_attributes

        # Another person notification
        other_user = create(:user)
        private_notification = other_user.notifications.create! valid_attributes

        set_auth(request, user)
        put :update, params: {id: private_notification.to_param,
                              notification: {read: true}}
        expect(response).to have_http_status(:forbidden)
        expect(response.content_type).to eq("application/json")
      end
    end

    context "normal user can update own notifications (mark as read)" do
      it "renders a JSON response with errors for the notification" do
        notification = user.notifications.create! valid_attributes

        set_auth(request, user)
        put :update, params: {
                       id: notification.to_param, notification: {
                         read: true,
                       },
                     }

        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe "DELETE #destroy" do
    it "normal user destroys owned requested notification" do
      notification = Notification.create! valid_attributes
      set_auth(request, user)
      expect {
        delete :destroy, params: {id: notification.id}
      }.to change(Notification, :count).by(-1)
      expect(response).to have_http_status(:no_content)
    end

    it "normal user can't destroy others notifications" do
      # Another person notification
      other_user = create(:user)
      private_notification = other_user.notifications.create! valid_attributes

      set_auth(request, user)
      expect {
        delete :destroy, params: {id: private_notification.id}
      }.to change(Notification, :count).by(0)
      expect(response).to have_http_status(:forbidden)
    end
  end
end
