Rails.application.routes.draw do
  mount Rswag::Ui::Engine => "/api-docs"
  mount Rswag::Api::Engine => "/api-docs"
  resources :notifications

  resources :users do
    resources :annotations
  end

  resources :courses do
    resources :reviews
    resources :videos do
      member do
        resources :study_materials
        resources :video_comments
      end
    end
  end

  resources :taken_courses

  post "auth/login" => "user_token#create"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
