class User < ApplicationRecord
  include AASM
  rolify
  before_create :set_uid
  before_create :set_email_token_verification
  after_create :assign_default_role
  after_create :send_email_verification

  has_secure_password
  has_one_attached :avatar

  # Relationships
  has_many :courses
  has_many :taken_courses
  has_many :study_materials
  has_many :notifications
  has_many :reviews
  has_many :annotations

  # Validations
  validates :username, uniqueness: true
  validates :email, uniqueness: true
  validates :name, presence: true
  validates :lastname, presence: true

  def is_owner?(resource)
    if resource.find_by(user_id: self.id)
      return true
    else
      return false
    end
  end

  def validate_email_token(token)
    self.verify_email! if self.email_verification_token == token
  end

  private

  # Manage email states
  aasm column: "email_state" do
    state :pending, initial: true
    state :verified
    state :rejected

    event :verify_email do
      transitions from: [:pending, :rejected], to: :verified
    end

    event :reject_email do
      transitions from: [:verified, :pending], to: :rejected
    end

    event :reset_email do
      transitions from: [:rejected, :verified], to: :pending
    end
  end

  # Set unique identifier after user creation
  def set_uid
    self.uid = SecureRandom.hex(11)
  end

  # Set email toker for verification
  def set_email_token_verification
    self.email_verification_token = SecureRandom.hex(10)
  end

  # Assign Student as default role
  def assign_default_role
    self.add_role(:student) if self.roles.blank?
  end

  # Send email verification to user
  def send_email_verification
    ValidationMailer.account_verification(self).deliver_now
  end
end
