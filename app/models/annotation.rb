class Annotation < ApplicationRecord
  belongs_to :user
  belongs_to :video

  # Model validations
  validates :comment, presence: true
end
