class StudyMaterial < ApplicationRecord
  belongs_to :user
  belongs_to :video

  has_many_attached :files

  # Validations
  validates :title, presence: true
  validates :url, presence: true
end
