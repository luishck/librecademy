class Notification < ApplicationRecord
  belongs_to :user

  # Model validations
  validates :header, presence: true
  validates :content, presence: true
end
