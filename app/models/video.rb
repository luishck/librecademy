class Video < ApplicationRecord
  belongs_to :course

  has_one_attached :cover
  has_many :study_materials
  has_many :video_comments
end
