class Course < ApplicationRecord
  include AASM

  belongs_to :user
  has_many :videos
  has_many :reviews
  has_many :reviewers, through: :reviews, source: :user
  has_many :taken_courses
  has_many :students, through: :taken_courses, source: :user

  accepts_nested_attributes_for :videos, allow_destroy: true

  has_one_attached :cover

  # Model validations
  validates :title, presence: true
  validates :description, presence: true
  validates :creator, presence: true

  # Search scopes
  scope :creator, -> (creator) { where creator: creator }
  scope :course_ids, -> (course_ids) { where id: course_ids }
  scope :title, -> (title) { where title: title }
  scope :verified, -> (verified) { where verified: verified }
  scope :top, -> (top) { where top: top }

  # State Machine
  aasm column: "state" do
    state :draft, initial: true
    state :published
    state :banned

    event :to_published do
      transitions from: [:draft, :banned], to: :published
    end

    event :to_banned do
      transitions from: [:draft, :published], to: :banned
    end

    event :to_drafted do
      transitions from: :published, to: :draft
    end
  end
end
