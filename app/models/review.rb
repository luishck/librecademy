class Review < ApplicationRecord
  belongs_to :user
  belongs_to :course

  # Model validations
  validates :rate, numericality: {
                     only_integer: true,
                     less_than_or_equal_to: 5,
                     greater_than_or_equal_to: 1,
                   }, presence: true
  validates :comment, presence: true
end
