class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:

    alias_action :read, :create, :read, :update, :destroy, to: :crud

    user ||= User.new # guest user (not logged in)

    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :creator
      # Courses
      can :crud, Course
      # Videos
      can :crud, Video

      # Notifications
      can [:update, :destroy], Notification
    elsif user.has_role? :student
      can :read, :all
      can [:update, :destroy], Notification

      # Course Reviews
      can :crud, Review
    end
  end
end
