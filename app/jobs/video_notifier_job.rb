class VideoNotifierJob < ApplicationJob
  queue_as :default

  def perform(course)
    NewVideoNotifier.notify_students course
  end
end
