class StudyMaterialSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :url, :git

  belongs_to :user
  belongs_to :video

  attribute :files do |object|
    object.files.map { |f| url_for(f) }
  end

  private

  def self.url_for(file)
    Rails.application.routes.url_helpers.rails_blob_url(file)
  end
end
