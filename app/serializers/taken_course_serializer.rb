class TakenCourseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :user_id, :course_id

  belongs_to :user
  belongs_to :course
end
