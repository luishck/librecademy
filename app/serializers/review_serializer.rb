class ReviewSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :rate, :comment, :state

  belongs_to :user
  belongs_to :course
end
