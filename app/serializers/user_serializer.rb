class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :lastname, :email, :username, :github, :email_state

  attribute :avatar do |object|
    if object.avatar.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object.avatar)
    else
      Rails.application.default_url_options[:host] + "/assets/user_default_avatar.png"
    end
  end

  attribute :roles do |object|
    object.roles.map { |r| r.name }
  end

  has_many :courses
  has_many :taken_courses
  has_many :study_materials
  has_many :notifications
  has_many :reviews
  has_many :annotations
end
