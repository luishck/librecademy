class VideoCommentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :content, :state, :top

  belongs_to :user
  belongs_to :video
end
