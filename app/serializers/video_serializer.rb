class VideoSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :title, :description, :author, :url, :duration, :state, :list_index

  belongs_to :course
  has_many :study_materials

  attribute :cover do |object|
    Rails.application.routes.url_helpers.rails_blob_url(object.cover) if object.cover.attached?
  end
end
