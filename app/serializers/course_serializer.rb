class CourseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :title, :description, :creator, :verified, :top, :tags

  has_many :videos

  attribute :cover do |object|
    if object.cover.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object.cover)
    else
      Rails.application.default_url_options[:host] + "/assets/sample-cover.jpg"
    end
  end
end
