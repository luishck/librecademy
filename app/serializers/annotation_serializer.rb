class AnnotationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :comment, :video_time

  belongs_to :user
  belongs_to :video
end
