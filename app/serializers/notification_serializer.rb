class NotificationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :header, :content, :kind, :read

  belongs_to :user
end
