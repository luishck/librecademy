class TakenCoursesController < ApplicationController
  before_action :authenticate_user
  before_action :set_taken_course, only: [:show, :update, :destroy]

  # GET /taken_courses
  def index
    @taken_courses = current_user.taken_courses.all

    render json: serialized_json(@taken_courses, {include: [:course, :user]})
  end

  # GET /taken_courses/1
  def show
    render json: serialized_json(@taken_course, {include: [:course, :user]})
  end

  # POST /taken_courses
  def create
    course = current_user.taken_courses.find_by(course_id: params[:taken_course][:course_id]).nil?
    unless course
      return render json: {message: "already created"}
    end

    @taken_course = current_user.taken_courses.new(taken_course_params)

    if @taken_course.save
      render json: serialized_json(@taken_course), status: :created
    else
      render json: @taken_course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /taken_courses/1
  def update
    if @taken_course.update(taken_course_params)
      render json: serialized_json(@taken_course)
    else
      render json: @taken_course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /taken_courses/1
  def destroy
    @taken_course.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_taken_course
    @taken_course = current_user.taken_courses.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def taken_course_params
    params.require(:taken_course).permit(:course_id, :state)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    TakenCourseSerializer.new(resource, options).serialized_json
  end
end
