class UsersController < ApplicationController
  before_action :authenticate_user, except: [:create]
  before_action :set_user, only: [:show, :update, :destroy]
  skip_authorize_resource only: :create

  # GET /users
  def index
    if current_user.has_any_role?(:admin, :moderator)
      @users = User.all
    else
      @users = current_user
    end

    render json: serialized_json(@users)
  end

  # GET /users/1
  def show
    render json: serialized_json(@user)
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: {jwt: client_token(@user).token, expires_at: Time.now + 24.hours}, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if handle_actions(params, @user) && @user.update(user_params)
      render json: serialized_json(@user)
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.required(:user).permit(
      :name,
      :lastname,
      :password,
      :avatar,
      :email,
      :username,
      :email_state,
      :github
    )
  end

  # Generate a token for new user
  def client_token(user)
    Knock::AuthToken.new payload: {sub: user.id}
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    UserSerializer.new(resource, options).serialized_json
  end

  # Handle param action requests
  def handle_actions(params, user_inst)
    # Skip actions if no one
    return true if !params.has_key? :req_action

    if params.has_key? :req_action
      # Get params
      req_action = params[:req_action]

      case req_action
      when "verify_email" # Request a email validation
        req_token = params[:email_verification_token]
        if current_user.validate_email_token(req_token)
          return true
        else
          user_inst.errors.add(
            :email_validation,
            I18n.t("validations.email_verification.cant_validate")
          )
          return false
        end
      when "request_creator" # Request Creator Role
        if current_user.verified?
          current_user.add_role(:creator)
          return true
        else
          user_inst.errors.add(
            :verification,
            I18n.t("validations.role_change.not_verified")
          )
          return false
        end
      end
    end
  end
end
