class NotificationsController < ApplicationController
  before_action :authenticate_user
  before_action :set_notification, only: [:show, :update, :destroy]
  load_and_authorize_resource

  # GET /notifications
  def index
    @notifications = current_user.notifications.all

    render json: serialized_json(@notifications)
  end

  # GET /notifications/1
  def show
    render json: serialized_json(@notification)
  end

  # POST /notifications
  def create
    @notification = Notification.new(notification_params)

    if @notification.save
      render json: (serialized_json @notification), status: :created
    else
      render json: @notification.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /notifications/1
  def update
    is_owner?(@notification, current_user)
    if @notification.update(notification_params)
      render json: serialized_json(@notification)
    else
      render json: @notification.errors, status: :unprocessable_entity
    end
  end

  # DELETE /notifications/1
  def destroy
    is_owner?(@notification, current_user)
    @notification.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_notification
    @notification = current_user.notifications.find_by_id(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def notification_params
    params.require(:notification).permit(:header, :content, :kind, :read, :picture, :user_id)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    NotificationSerializer.new(resource, options).serialized_json
  end
end
