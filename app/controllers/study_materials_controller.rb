class StudyMaterialsController < ApplicationController
  before_action :authenticate_user
  before_action :set_study_material, only: [:show, :update, :destroy]

  # GET /study_materials
  def index
    @study_materials = StudyMaterial.all

    render json: serialized_json(@study_materials)
  end

  # GET /study_materials/1
  def show
    render json: serialized_json(@study_material)
  end

  # POST /study_materials
  def create
    @video = Video.find(params[:id])
    @study_material = @video.study_materials.new(study_material_params)
    @study_material.user = current_user

    if @study_material.save
      render json: serialized_json(@study_material), status: :created
    else
      render json: @study_material.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /study_materials/1
  def update
    if @study_material.update(study_material_params)
      render json: serialized_json(@study_material)
    else
      render json: @study_material.errors, status: :unprocessable_entity
    end
  end

  # DELETE /study_materials/1
  def destroy
    @study_material.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_study_material
    @study_material = StudyMaterial.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def study_material_params
    params.require(:study_material).permit(:title, :description, :url, :git, :files)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    StudyMaterialSerializer.new(resource, options).serialized_json
  end
end
