class ApplicationController < ActionController::API
  include Knock::Authenticable
  include PropertyValidations

  private

  # Rescue from cancan access denegation
  rescue_from CanCan::AccessDenied do |exception|
    Rails.logger.error(exception)
    render json: error_message(
             "Access Denied",
             "You are not authorized for this action",
             412
           ), status: :forbidden
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    Rails.logger.error(exception)
    render json: error_message(
             "Not Found",
             "Requested item can't be found",
             404
           ), status: 404
  end

  def error_message(title, detail, status)
    return {
             "errors": [
               {
                 "status": status,
                 "title": title,
                 "detail": detail,
               },
             ],
           }
  end
end
