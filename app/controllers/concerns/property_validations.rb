module PropertyValidations
  extend ActiveSupport::Concern

  # check whether the owner
  def is_owner?(resource, owner)
    return true if can_skip?
    unless resource[key_name(owner)] == owner.id
      raise CanCan::AccessDenied
    end
  end

  private

  def key_name(owner)
    name = "#{owner.class.name.demodulize.downcase}_id"
    return name.parameterize.underscore.to_sym
  end

  def can_skip?
    # If user is an admin, skip this validation
    return current_user.has_any_role? :admin, :moderator
  end
end
