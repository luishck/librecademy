class VideoCommentsController < ApplicationController
  before_action :set_video_comment, only: [:show, :update, :destroy]

  # GET /video_comments
  def index
    @video_comments = VideoComment.all

    render json: @video_comments
  end

  # GET /video_comments/1
  def show
    render json: @video_comment
  end

  # POST /video_comments
  def create
    video = Video.find(params[:id])
    @video_comment = video.video_comments.new(video_comment_params)
    @video_comment.user = current_user

    if @video_comment.save
      render json: @video_comment, status: :created
    else
      render json: @video_comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /video_comments/1
  def update
    if @video_comment.update(video_comment_params)
      render json: @video_comment
    else
      render json: @video_comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /video_comments/1
  def destroy
    @video_comment.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_video_comment
    video = Video.find(params[:video_id])
    @video_comment = video.video_comments.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def video_comment_params
    params.require(:video_comment).permit(:content, :state)
  end
end
