class CoursesController < ApplicationController
  before_action :authenticate_user
  load_and_authorize_resource
  before_action :set_course, only: [:update, :destroy]

  # GET /courses
  def index
    # check if valid filters are applied
    if is_filterable?(filtering_params(params))
      @courses = Course.where(nil)
      filtering_params(params).each do |key, value|
        @courses = @courses.public_send(key, value) if value.present?
      end
    else
      @courses = Course.where(top: true)
    end

    render json: serialized_json(@courses)
  end

  # GET /courses/1
  def show
    @course = Course.find(params[:id])
    render json: serialized_json(@course, {include: [:videos]})
  end

  # POST /courses
  def create
    @course = current_user.courses.new(course_params)

    if @course.save
      render json: serialized_json(@course), status: :created, location: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /courses/1
  def update
    # Only owner can make updates
    is_owner?(@course, current_user)

    # Validates request action
    handle_state_action(@course, params[:state_action])

    if @course.update(course_params)
      render json: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /courses/1
  def destroy
    is_owner?(@course, current_user)
    @course.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_course
    @course = Course.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def course_params
    valid_params = []
    video_params = [
      :title,
      :description,
      :list_index,
      :url,
      :duration,
      :author,
      :course_id,
    ]

    # Allow certain params only for admin
    if current_user.has_role? :admin
      valid_params = [
        :title,
        :description,
        :cover,
        :user_id,
        :top,
        :creator,
        :verified,
        :tags,
        videos_attributes: video_params,
      ]
    else
      valid_params = [
        :title,
        :description,
        :cover,
        :creator,
        :tags,
        videos_attributes: video_params,
      ]
    end

    params.require(:course).permit(valid_params)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    CourseSerializer.new(resource, options).serialized_json
  end

  def filtering_params(params)
    params.slice(:course_ids, :creator, :verified, :top)
  end

  def is_filterable?(params)
    return true if params.has_key?(:course_ids) || params.has_key?(:creator) || params.has_key?(:verified) || params.has_key?(:top)
  end

  def handle_state_action(course, action)
    admin = current_user.has_any_role? :admin, :moderator
    ## Only admin can bann a course
    if admin && action == "to_banned!"
      course.public_send(action)
    elsif action == "to_published!" || action == "to_drafted"
      course.public_send(action)
    end
  end
end
