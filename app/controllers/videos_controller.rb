class VideosController < ApplicationController
  load_and_authorize_resource

  before_action :set_video, only: [:show, :update, :destroy]

  # GET /videos
  def index
    @videos = Course.find(params[:course_id]).videos

    render json: serialized_json(@videos)
  end

  # GET /videos/1
  def show
    @video = Course.find(params[:course_id]).videos.find(params[:id])
    render json: serialized_json(@video, {include: [:study_materials, :video_comments]})
  end

  # POST /videos
  def create
    @video = Video.new(video_params)

    if @video.save
      render json: serialized_json(@video), status: :created, location: @video
    else
      render json: serialized_json(@video.errors), status: :unprocessable_entity
    end
  end

  # PATCH/PUT /videos/1
  def update
    if @video.update(video_params)
      render json: serialized_json(@video)
    else
      render json: serialized_json(@video.errors), status: :unprocessable_entity
    end
  end

  # DELETE /videos/1
  def destroy
    @video.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_video
    @video = current_user.courses.find(params[:course_id]).videos.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def video_params
    params.require(:video).permit(:title, :description, :list_index, :url, :duration, :author, :course_id, :cover)
  end

  # shortand for serializer
  def serialized_json(resource, options = {})
    VideoSerializer.new(resource, options).serialized_json
  end
end
