class AnnotationsController < ApplicationController
  before_action :set_annotation, only: [:show, :update, :destroy]

  # GET /annotations
  def index
    @annotations = Annotation.all

    render json: serialized_json(@annotations)
  end

  # GET /annotations/1
  def show
    render json: serialized_json(@annotation)
  end

  # POST /annotations
  def create
    @annotation = current_user.annotations.new(annotation_params)

    if @annotation.save
      render json: serialized_json(@annotation), status: :created
    else
      render json: @annotation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /annotations/1
  def update
    is_owner?(@annotation, current_user)
    if @annotation.update(annotation_params)
      render json: serialized_json(@annotation)
    else
      render json: @annotation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /annotations/1
  def destroy
    is_owner?(@annotation, current_user)
    @annotation.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_annotation
    @annotation = Annotation.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def annotation_params
    params.require(:annotation).permit(:comment, :video_time, :video_id)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    AnnotationSerializer.new(resource, options).serialized_json
  end
end
