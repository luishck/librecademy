class ReviewsController < ApplicationController
  load_and_authorize_resource
  before_action :set_review, only: [:show, :update, :destroy]

  # GET /reviews
  def index
    @reviews = Review.all

    render json: serialized_json(@reviews)
  end

  # GET /reviews/1
  def show
    render json: serialized_json(@review)
  end

  # POST /reviews
  def create
    @course = Course.find(params[:course_id])
    @review = current_user.reviews.new(review_params)
    @review.course = @course

    if @review.save
      render json: serialized_json(@review), status: :created
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /reviews/1
  def update
    is_owner? @review, current_user
    if @review.update(review_params)
      render json: serialized_json(@review)
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # DELETE /reviews/1
  def destroy
    is_owner? @review, current_user
    @review.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_review
    @review = Review.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def review_params
    params.require(:review).permit(:rate, :comment, :state, :user_id, :course_id)
  end

  # Shortand for Json serializer
  def serialized_json(resource, options = {})
    ReviewSerializer.new(resource, options).serialized_json
  end
end
