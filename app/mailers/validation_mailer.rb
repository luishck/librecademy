class ValidationMailer < ApplicationMailer
  default from: "librecademy@gmail.com"

  def account_verification(user)
    @user = user
    @mail = {
      heading: I18n.t("mailer.account_verification.heading"),
      body: I18n.t("mailer.account_verification.body", user: @user.name),
      token: @user.email_verification_token,
      footer: I18n.t("mailer.defaults.footer"),
    }
    mail(
      to: @user.email,
      subject: I18n.t("mailer.account_verification.subject"),
    )
  end
end
