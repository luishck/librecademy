class NewVideoNotifier
  def self.notify_students(course)
    # Create a notification for each student
    begin
      course.students.each do |student|
        student.notifications.create(
          header: I18n.t("notifications.new_video.header", course: course.title),
          content: I18n.t("notifications.new_video.content", video: course.videos.last.title),
          kind: "info",
        )
      end
      return true
    rescue => exception
      raise exception
    end
  end
end
