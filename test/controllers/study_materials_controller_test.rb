require 'test_helper'

class StudyMaterialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @study_material = study_materials(:one)
  end

  test "should get index" do
    get study_materials_url, as: :json
    assert_response :success
  end

  test "should create study_material" do
    assert_difference('StudyMaterial.count') do
      post study_materials_url, params: { study_material: { description: @study_material.description, files: @study_material.files, git: @study_material.git, title: @study_material.title, url: @study_material.url, user_id: @study_material.user_id, video_id: @study_material.video_id } }, as: :json
    end

    assert_response 201
  end

  test "should show study_material" do
    get study_material_url(@study_material), as: :json
    assert_response :success
  end

  test "should update study_material" do
    patch study_material_url(@study_material), params: { study_material: { description: @study_material.description, files: @study_material.files, git: @study_material.git, title: @study_material.title, url: @study_material.url, user_id: @study_material.user_id, video_id: @study_material.video_id } }, as: :json
    assert_response 200
  end

  test "should destroy study_material" do
    assert_difference('StudyMaterial.count', -1) do
      delete study_material_url(@study_material), as: :json
    end

    assert_response 204
  end
end
